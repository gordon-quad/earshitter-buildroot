#!/bin/sh

dbus-monitor --system --pcap 'interface=org.bluealsa.Manager1,member=PCMAdded' 'interface=org.bluealsa.Manager1,member=PCMRemoved' | python3 -u /etc/cancer/dump_dbus_pcap.py | 
    while read LINE; do
        # /org/bluealsa org.bluealsa.Manager1 PCMAdded /org/bluealsa/hci0/dev_F4_4E_FD_8C_A5_AD/a2dp
        # /org/bluealsa org.bluealsa.Manager1 PCMRemoved /org/bluealsa/hci0/dev_F4_4E_FD_8C_A5_AD/a2dp
        DBUS_ADDR=`echo $LINE | cut -d' ' -f 1-3`
        DBUS_OBJECT=`echo $LINE | cut -d' ' -f 4`
        case $DBUS_ADDR in
            "/org/bluealsa org.bluealsa.Manager1 PCMAdded")
                DEV_ADDR=`echo $DBUS_OBJECT | cut -d/ -f 5`
                DEV_PROFILE=`echo $DBUS_OBJECT | cut -d/ -f 6`
                if [ "$DEV_PROFILE" == "a2dp" ];
                then
                    echo "defaults.bluealsa.device $DEV_ADDR" | sed 's/dev_//;s/_/:/g' > /tmp/asound-device
                    start-stop-daemon --start --quiet --background --exec /usr/bin/mpd
                fi
                ;;
            "/org/bluealsa org.bluealsa.Manager1 PCMRemoved")
                DEV_ADDR=`echo $DBUS_OBJECT | cut -d/ -f 5`
                DEV_PROFILE=`echo $DBUS_OBJECT | cut -d/ -f 6`
                if [ "$DEV_PROFILE" == "a2dp" ];
                then
                    echo rm /tmp/asound-device
                    start-stop-daemon --stop --quiet --pidfile /var/run/mpd.pid
                fi
                ;;
        esac
    done
