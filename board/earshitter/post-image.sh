#!/bin/bash

BOARD_DIR="$(dirname $0)"
BOOT_DIR=${BINARIES_DIR}/boot

mkdir -p ${BOOT_DIR}

cp    ${BOARD_DIR}/cmdline.txt                  ${BOOT_DIR}
cp    ${BOARD_DIR}/config.txt                   ${BOOT_DIR}
cp    ${BINARIES_DIR}/bcm2708-rpi-zero-w.dtb    ${BOOT_DIR}
cp    ${BINARIES_DIR}/rpi-firmware/bootcode.bin ${BOOT_DIR}
cp    ${BINARIES_DIR}/rpi-firmware/fixup.dat    ${BOOT_DIR}
cp    ${BINARIES_DIR}/rpi-firmware/start.elf    ${BOOT_DIR}
cp -r ${BINARIES_DIR}/rpi-firmware/overlays     ${BOOT_DIR}
cp    ${BINARIES_DIR}/zImage                    ${BOOT_DIR}
ln    ${BINARIES_DIR}/rootfs.squashfs           ${BOOT_DIR}

