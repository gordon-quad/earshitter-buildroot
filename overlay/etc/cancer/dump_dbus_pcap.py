#!/usr/bin/python3

import struct
import sys

def pad(o):
    return o - (o % 8) + (0 if o % 8 == 0 else 8)

def read_small_str(packet, o):
    slen = packet[o]
    o += 1
    return (packet[o:o+slen].decode('utf-8'), o + slen + 1)

def read_str(packet, o):
    slen, = struct.unpack('<L', packet[o:o+4])
    o += 4
    return (packet[o:o+slen].decode('utf-8'), o + slen + 1)

def parse_dbus_header(packet, o):
    code = packet[o]
    o += 1
    (t, o) = read_small_str(packet, o)
    if t == 'u':
        o += 4
        o = pad(o)
        return (code, struct.unpack('<L', packet[o:o+4]), o)
    elif t == 'g':
        (s, o) = read_small_str(packet,o)
        o = pad(o)
        return (code, s, o)
    else:
        assert(t == 'o' or t == 's')
        (s, o) = read_str(packet,o)
        o = pad(o)
        return (code, s, o)

def parse_dbus_packet(packet):
    endianness = chr(packet[0])
    assert(endianness == 'l')
    msg_type = packet[1]
    flags = packet[2]
    proto_ver = packet[3]
    body_len, = struct.unpack('<L', packet[4:8])
    serial, = struct.unpack('<L', packet[8:12])
    fields_len, = struct.unpack('<L', packet[12:16])
    fl = fields_len
    i = 16
    headers = {}
    while i < (fields_len + 16):
        (t, val, i) = parse_dbus_header(packet, i)
        headers[t] = val
    (str_body, i) = read_str(packet, i)
    return {
            'endianness': endianness,
            'msg_type': msg_type,
            'proto_ver': proto_ver,
            'body_len': body_len,
            'fields_len': fields_len,
            'headers': headers,
            'body': str_body
            }

def dump_dbus_pcap(fd):
    # We don't give a fuck about header
    fd.read(24)
    while True:
        header = fd.read(16)
        if not header:
            return
        (ts_sec, ts_usec, incl_len, orig_len) = struct.unpack('<LLLL', header)
        #print((ts_sec, ts_usec, incl_len, orig_len))
        body = fd.read(incl_len)
        if not body:
            return
        p = parse_dbus_packet(body)
        print(p['headers'][1], p['headers'][2], p['headers'][3], p['body'])

dump_dbus_pcap(sys.stdin.buffer)
